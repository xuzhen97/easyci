# easyci
> 最精简ci工具。


支持参数 --file, -f 脚本名称， --params,-s 脚本所需参数, --port,-p 钩子运行的端口，默认8080。

此工具会增加get / post /hook地址，使用8080端口，调用则异步执行脚本，会将参数传给脚本，请根据需求实现脚本。

在给脚本传入参数时`$1` 固定会传入header json, `$2` 会传入body。

```bash
#!/bin/bash
# 如果时json的话，可以使用jq类库配合脚本解析，做一些自动化的操作。

# header
echo $1
echo "-----------------------------------"
# body
echo $2
```

最后不要忘记给脚本可执行权限, 例如其下：

```bash
chmod +x deploy.sh
```

shell脚本路径必须传入，否则程序无法运行。

## 先准备下基础配置
> 自己鼓捣的脚本
```bash
sudo curl -o /opt/.importrc https://gitee.com/xuzhen97/ops/raw/master/importrc
```

编写bash脚本之前可以执行, 主要是为了使用公共函数，里面封装了发送钉钉消息的功能等。
```bash
source /opt/.importrc
import https://gitee.com/xuzhen97/ops/raw/master/common/common_fun.sh
```

## 应用场景举例
> 及其提前配置好公用的脚本环境，主要是为了节省shell的脚本编写行数, easyci需要安装go环境进行编译。
> 脚本中lock主要是就是写出个文件用于判断，防止同时有人合并指定多次，这样会一次一次执行，问题就少了很多，当然还是不要经常合并代码的好。

dapeng-deploy.sh
```bash
#!/bin/bash

# 导入可以加载远程脚本的指令
source /opt/.importrc
# 导入公共函数模块
import https://gitee.com/xuzhen97/ops/raw/master/common/common_fun.sh

# 发布到测试环境逻辑，请自己配置重新部署的webhook
publishDev(){
  local dir="/tmp/publish-dapeng"
  rm -rf $dir
  git clone -b test --depth=1 ssh://git@gitlab.dapeng.lan:30002/application-one/pc-dapeng/dapeng.git $dir
  cd $dir
  mvn clean package -Dmaven.test.skip=true
  docker build -t registry.cn-beijing.aliyuncs.com/jinan/dapeng-pc:latest .
  docker push registry.cn-beijing.aliyuncs.com/jinan/dapeng-pc:latest
  msg="ci通知: dapeng项目发布测试镜像完毕，会触发k8s重新部署钩子，请稍后重试。"
  logInfo "$msg"
  sendMsgDingtalk "$msg"
  rm -rf $dir
}

event=$(echo $2 | jq .event_type)
branch=$(echo $2 | jq .object_attributes.target_branch)
state=$(echo $2 | jq .object_attributes.state)

# 判断事件是否时merge事件,如果是才执行
if [ "$event" = '"merge_request"' ]; then
  # 判断合并的分支是否是dev, 且是合并后执行
  if [ "$branch" = '"test"' ] && [ "$state" = '"merged"' ]; then
    lock
    publishDev 
    unlock
  fi
fi

```

命令执行：
```bash
nohup ./easyci -f ./dapeng-deploy.sh -p 9992 &
```

gitlab配置钩子, 基础资料
```
新创建

{"object_kind":"merge_request","event_type":"merge_request","user":{"name":"徐震","username":"xuzhen97","avatar_url":"http://gitlab.dapeng.lan/uploads/-/system/user/avatar/32/avatar.png"},"project":{"id":129,"name":"dapeng","description":"主站项目","web_url":"http://gitlab.dapeng.lan/application-one/pc-dapeng/dapeng","avatar_url":"http://gitlab.dapeng.lan/uploads/-/system/project/avatar/129/34758D75-ADA1-49b1-950E-D5DD1737E194.png","git_ssh_url":"ssh://git@gitlab.dapeng.lan:30002/application-one/pc-dapeng/dapeng.git","git_http_url":"http://gitlab.dapeng.lan/application-one/pc-dapeng/dapeng.git","namespace":"老版本PC主站项目组","visibility_level":0,"path_with_namespace":"application-one/pc-dapeng/dapeng","default_branch":"master","ci_config_path":null,"homepage":"http://gitlab.dapeng.lan/application-one/pc-dapeng/dapeng","url":"ssh://git@gitlab.dapeng.lan:30002/application-one/pc-dapeng/dapeng.git","ssh_url":"ssh://git@gitlab.dapeng.lan:30002/application-one/pc-dapeng/dapeng.git","http_url":"http://gitlab.dapeng.lan/application-one/pc-dapeng/dapeng.git"},"object_attributes":{"assignee_id":null,"author_id":32,"created_at":"2020-08-19 16:08:55 +0800","description":"","head_pipeline_id":null,"id":4776,"iid":654,"last_edited_at":null,"last_edited_by_id":null,"merge_commit_sha":null,"merge_error":null,"merge_params":{"force_remove_source_branch":"0"},"merge_status":"unchecked","merge_user_id":null,"merge_when_pipeline_succeeds":false,"milestone_id":null,"source_branch":"testhook","source_project_id":129,"state":"opened","target_branch":"test","target_project_id":129,"time_estimate":0,"title":"修改footer测试","updated_at":"2020-08-19 16:08:55 +0800","updated_by_id":null,"url":"http://gitlab.dapeng.lan/application-one/pc-dapeng/dapeng/merge_requests/654","source":{"id":129,"name":"dapeng","description":"主站项目","web_url":"http://gitlab.dapeng.lan/application-one/pc-dapeng/dapeng","avatar_url":"http://gitlab.dapeng.lan/uploads/-/system/project/avatar/129/34758D75-ADA1-49b1-950E-D5DD1737E194.png","git_ssh_url":"ssh://git@gitlab.dapeng.lan:30002/application-one/pc-dapeng/dapeng.git","git_http_url":"http://gitlab.dapeng.lan/application-one/pc-dapeng/dapeng.git","namespace":"老版本PC主站项目组","visibility_level":0,"path_with_namespace":"application-one/pc-dapeng/dapeng","default_branch":"master","ci_config_path":null,"homepage":"http://gitlab.dapeng.lan/application-one/pc-dapeng/dapeng","url":"ssh://git@gitlab.dapeng.lan:30002/application-one/pc-dapeng/dapeng.git","ssh_url":"ssh://git@gitlab.dapeng.lan:30002/application-one/pc-dapeng/dapeng.git","http_url":"http://gitlab.dapeng.lan/application-one/pc-dapeng/dapeng.git"},"target":{"id":129,"name":"dapeng","description":"主站项目","web_url":"http://gitlab.dapeng.lan/application-one/pc-dapeng/dapeng","avatar_url":"http://gitlab.dapeng.lan/uploads/-/system/project/avatar/129/34758D75-ADA1-49b1-950E-D5DD1737E194.png","git_ssh_url":"ssh://git@gitlab.dapeng.lan:30002/application-one/pc-dapeng/dapeng.git","git_http_url":"http://gitlab.dapeng.lan/application-one/pc-dapeng/dapeng.git","namespace":"老版本PC主站项目组","visibility_level":0,"path_with_namespace":"application-one/pc-dapeng/dapeng","default_branch":"master","ci_config_path":null,"homepage":"http://gitlab.dapeng.lan/application-one/pc-dapeng/dapeng","url":"ssh://git@gitlab.dapeng.lan:30002/application-one/pc-dapeng/dapeng.git","ssh_url":"ssh://git@gitlab.dapeng.lan:30002/application-one/pc-dapeng/dapeng.git","http_url":"http://gitlab.dapeng.lan/application-one/pc-dapeng/dapeng.git"},"last_commit":{"id":"5eb599f39e68b236501dc1f085fabd2948e028b8","message":"修改footer测试\n","timestamp":"2020-08-19T15:59:33+08:00","url":"http://gitlab.dapeng.lan/application-one/pc-dapeng/dapeng/commit/5eb599f39e68b236501dc1f085fabd2948e028b8","author":{"name":"xuzhen97","email":"xuzhen97@outlook.com"}},"work_in_progress":false,"total_time_spent":0,"human_total_time_spent":null,"human_time_estimate":null,"assignee_ids":[],"action":"open"},"labels":[],"changes":{"author_id":{"previous":null,"current":32},"created_at":{"previous":null,"current":"2020-08-19 16:08:55 +0800"},"description":{"previous":null,"current":""},"id":{"previous":null,"current":4776},"iid":{"previous":null,"current":654},"merge_params":{"previous":{},"current":{"force_remove_source_branch":"0"}},"source_branch":{"previous":null,"current":"testhook"},"source_project_id":{"previous":null,"current":129},"target_branch":{"previous":null,"current":"test"},"target_project_id":{"previous":null,"current":129},"title":{"previous":null,"current":"修改footer测试"},"updated_at":{"previous":null,"current":"2020-08-19 16:08:55 +0800"},"total_time_spent":{"previous":null,"current":0}},"repository":{"name":"dapeng","url":"ssh://git@gitlab.dapeng.lan:30002/application-one/pc-dapeng/dapeng.git","description":"主站项目","homepage":"http://gitlab.dapeng.lan/application-one/pc-dapeng/dapeng"}}
"unchecked"

关闭
{"object_kind":"merge_request","event_type":"merge_request","user":{"name":"徐震","username":"xuzhen97","avatar_url":"http://gitlab.dapeng.lan/uploads/-/system/user/avatar/32/avatar.png"},"project":{"id":129,"name":"dapeng","description":"主站项目","web_url":"http://gitlab.dapeng.lan/application-one/pc-dapeng/dapeng","avatar_url":"http://gitlab.dapeng.lan/uploads/-/system/project/avatar/129/34758D75-ADA1-49b1-950E-D5DD1737E194.png","git_ssh_url":"ssh://git@gitlab.dapeng.lan:30002/application-one/pc-dapeng/dapeng.git","git_http_url":"http://gitlab.dapeng.lan/application-one/pc-dapeng/dapeng.git","namespace":"老版本PC主站项目组","visibility_level":0,"path_with_namespace":"application-one/pc-dapeng/dapeng","default_branch":"master","ci_config_path":null,"homepage":"http://gitlab.dapeng.lan/application-one/pc-dapeng/dapeng","url":"ssh://git@gitlab.dapeng.lan:30002/application-one/pc-dapeng/dapeng.git","ssh_url":"ssh://git@gitlab.dapeng.lan:30002/application-one/pc-dapeng/dapeng.git","http_url":"http://gitlab.dapeng.lan/application-one/pc-dapeng/dapeng.git"},"object_attributes":{"assignee_id":null,"author_id":32,"created_at":"2020-08-19 16:08:55 +0800","description":"","head_pipeline_id":null,"id":4776,"iid":654,"last_edited_at":null,"last_edited_by_id":null,"merge_commit_sha":null,"merge_error":null,"merge_params":{"force_remove_source_branch":"0"},"merge_status":"can_be_merged","merge_user_id":null,"merge_when_pipeline_succeeds":false,"milestone_id":null,"source_branch":"testhook","source_project_id":129,"state":"closed","target_branch":"test","target_project_id":129,"time_estimate":0,"title":"修改footer测试","updated_at":"2020-08-19 16:09:57 +0800","updated_by_id":null,"url":"http://gitlab.dapeng.lan/application-one/pc-dapeng/dapeng/merge_requests/654","source":{"id":129,"name":"dapeng","description":"主站项目","web_url":"http://gitlab.dapeng.lan/application-one/pc-dapeng/dapeng","avatar_url":"http://gitlab.dapeng.lan/uploads/-/system/project/avatar/129/34758D75-ADA1-49b1-950E-D5DD1737E194.png","git_ssh_url":"ssh://git@gitlab.dapeng.lan:30002/application-one/pc-dapeng/dapeng.git","git_http_url":"http://gitlab.dapeng.lan/application-one/pc-dapeng/dapeng.git","namespace":"老版本PC主站项目组","visibility_level":0,"path_with_namespace":"application-one/pc-dapeng/dapeng","default_branch":"master","ci_config_path":null,"homepage":"http://gitlab.dapeng.lan/application-one/pc-dapeng/dapeng","url":"ssh://git@gitlab.dapeng.lan:30002/application-one/pc-dapeng/dapeng.git","ssh_url":"ssh://git@gitlab.dapeng.lan:30002/application-one/pc-dapeng/dapeng.git","http_url":"http://gitlab.dapeng.lan/application-one/pc-dapeng/dapeng.git"},"target":{"id":129,"name":"dapeng","description":"主站 项目","web_url":"http://gitlab.dapeng.lan/application-one/pc-dapeng/dapeng","avatar_url":"http://gitlab.dapeng.lan/uploads/-/system/project/avatar/129/34758D75-ADA1-49b1-950E-D5DD1737E194.png","git_ssh_url":"ssh://git@gitlab.dapeng.lan:30002/application-one/pc-dapeng/dapeng.git","git_http_url":"http://gitlab.dapeng.lan/application-one/pc-dapeng/dapeng.git","namespace":"老版本PC主站项目组","visibility_level":0,"path_with_namespace":"application-one/pc-dapeng/dapeng","default_branch":"master","ci_config_path":null,"homepage":"http://gitlab.dapeng.lan/application-one/pc-dapeng/dapeng","url":"ssh://git@gitlab.dapeng.lan:30002/application-one/pc-dapeng/dapeng.git","ssh_url":"ssh://git@gitlab.dapeng.lan:30002/application-one/pc-dapeng/dapeng.git","http_url":"http://gitlab.dapeng.lan/application-one/pc-dapeng/dapeng.git"},"last_commit":{"id":"5eb599f39e68b236501dc1f085fabd2948e028b8","message":"修改footer测试\n","timestamp":"2020-08-19T15:59:33+08:00","url":"http://gitlab.dapeng.lan/application-one/pc-dapeng/dapeng/commit/5eb599f39e68b236501dc1f085fabd2948e028b8","author":{"name":"xuzhen97","email":"xuzhen97@outlook.com"}},"work_in_progress":false,"total_time_spent":0,"human_total_time_spent":null,"human_time_estimate":null,"assignee_ids":[],"action":"close"},"labels":[],"changes":{"state":{"previous":"opened","current":"closed"},"updated_at":{"previous":"2020-08-19 16:08:55 +0800","current":"2020-08-19 16:09:57 +0800"},"total_time_spent":{"previous":null,"current":0}},"repository":{"name":"dapeng","url":"ssh://git@gitlab.dapeng.lan:30002/application-one/pc-dapeng/dapeng.git","description":"主站项目","homepage":"http://gitlab.dapeng.lan/application-one/pc-dapeng/dapeng"}}
JSON.object_attributes.state "closed"

合并
{"object_kind":"merge_request","event_type":"merge_request","user":{"name":"徐震","username":"xuzhen97","avatar_url":"http://gitlab.dapeng.lan/uploads/-/system/user/avatar/32/avatar.png"},"project":{"id":129,"name":"dapeng","description":"主站项目","web_url":"http://gitlab.dapeng.lan/application-one/pc-dapeng/dapeng","avatar_url":"http://gitlab.dapeng.lan/uploads/-/system/project/avatar/129/34758D75-ADA1-49b1-950E-D5DD1737E194.png","git_ssh_url":"ssh://git@gitlab.dapeng.lan:30002/application-one/pc-dapeng/dapeng.git","git_http_url":"http://gitlab.dapeng.lan/application-one/pc-dapeng/dapeng.git","namespace":"老版本PC主站项目组","visibility_level":0,"path_with_namespace":"application-one/pc-dapeng/dapeng","default_branch":"master","ci_config_path":null,"homepage":"http://gitlab.dapeng.lan/application-one/pc-dapeng/dapeng","url":"ssh://git@gitlab.dapeng.lan:30002/application-one/pc-dapeng/dapeng.git","ssh_url":"ssh://git@gitlab.dapeng.lan:30002/application-one/pc-dapeng/dapeng.git","http_url":"http://gitlab.dapeng.lan/application-one/pc-dapeng/dapeng.git"},"object_attributes":{"assignee_id":null,"author_id":32,"created_at":"2020-08-19 08:13:40 UTC","description":"","head_pipeline_id":null,"id":4779,"iid":655,"last_edited_at":null,"last_edited_by_id":null,"merge_commit_sha":"db4d7e6f9d4a1eeb7c3d8ced3303e8bee787dcf3","merge_error":null,"merge_params":{"force_remove_source_branch":"0"},"merge_status":"can_be_merged","merge_user_id":null,"merge_when_pipeline_succeeds":false,"milestone_id":null,"source_branch":"testhook","source_project_id":129,"state":"merged","target_branch":"test","target_project_id":129,"time_estimate":0,"title":"修改footer测试","updated_at":"2020-08-19 08:13:46 UTC","updated_by_id":null,"url":"http://gitlab.dapeng.lan/application-one/pc-dapeng/dapeng/merge_requests/655","source":{"id":129,"name":"dapeng","description":"主站项目","web_url":"http://gitlab.dapeng.lan/application-one/pc-dapeng/dapeng","avatar_url":"http://gitlab.dapeng.lan/uploads/-/system/project/avatar/129/34758D75-ADA1-49b1-950E-D5DD1737E194.png","git_ssh_url":"ssh://git@gitlab.dapeng.lan:30002/application-one/pc-dapeng/dapeng.git","git_http_url":"http://gitlab.dapeng.lan/application-one/pc-dapeng/dapeng.git","namespace":"老版本PC主站项目组","visibility_level":0,"path_with_namespace":"application-one/pc-dapeng/dapeng","default_branch":"master","ci_config_path":null,"homepage":"http://gitlab.dapeng.lan/application-one/pc-dapeng/dapeng","url":"ssh://git@gitlab.dapeng.lan:30002/application-one/pc-dapeng/dapeng.git","ssh_url":"ssh://git@gitlab.dapeng.lan:30002/application-one/pc-dapeng/dapeng.git","http_url":"http://gitlab.dapeng.lan/application-one/pc-dapeng/dapeng.git"},"target":{"id":129,"name":"dapeng","description":"主站项目","web_url":"http://gitlab.dapeng.lan/application-one/pc-dapeng/dapeng","avatar_url":"http://gitlab.dapeng.lan/uploads/-/system/project/avatar/129/34758D75-ADA1-49b1-950E-D5DD1737E194.png","git_ssh_url":"ssh://git@gitlab.dapeng.lan:30002/application-one/pc-dapeng/dapeng.git","git_http_url":"http://gitlab.dapeng.lan/application-one/pc-dapeng/dapeng.git","namespace":"老版本PC主站 项目组","visibility_level":0,"path_with_namespace":"application-one/pc-dapeng/dapeng","default_branch":"master","ci_config_path":null,"homepage":"http://gitlab.dapeng.lan/application-one/pc-dapeng/dapeng","url":"ssh://git@gitlab.dapeng.lan:30002/application-one/pc-dapeng/dapeng.git","ssh_url":"ssh://git@gitlab.dapeng.lan:30002/application-one/pc-dapeng/dapeng.git","http_url":"http://gitlab.dapeng.lan/application-one/pc-dapeng/dapeng.git"},"last_commit":{"id":"5eb599f39e68b236501dc1f085fabd2948e028b8","message":"修改footer测试\n","timestamp":"2020-08-19T15:59:33+08:00","url":"http://gitlab.dapeng.lan/application-one/pc-dapeng/dapeng/commit/5eb599f39e68b236501dc1f085fabd2948e028b8","author":{"name":"xuzhen97","email":"xuzhen97@outlook.com"}},"work_in_progress":false,"total_time_spent":0,"human_total_time_spent":null,"human_time_estimate":null,"assignee_ids":[],"action":"merge"},"labels":[],"changes":{"state":{"previous":"locked","current":"merged"},"updated_at":{"previous":"2020-08-19 08:13:46 UTC","current":"2020-08-19 08:13:46 UTC"},"total_time_spent":{"previous":null,"current":0}},"repository":{"name":"dapeng","url":"ssh://git@gitlab.dapeng.lan:30002/application-one/pc-dapeng/dapeng.git","description":"主站项目","homepage":"http://gitlab.dapeng.lan/application-one/pc-dapeng/dapeng"}}
JSON.object_attributes.state merged
```

看gitlab的调用情况，意思是我们自己在脚本里面判断决定逻辑如何处理，可以参考脚本，注意的是我们使用了库jq

在gitlab中配置钩子，加入ip为192.168.1.6,那么就配置地址,请求方式为post.
```
http://192.168.1.6:9992/hook
```
