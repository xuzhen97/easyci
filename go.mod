module easyci

go 1.13

require (
	github.com/gin-gonic/gin v1.6.3
	github.com/urfave/cli/v2 v2.2.0
)
