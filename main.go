package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/exec"
	"path/filepath"
	"strings"
	"io/ioutil"

	"github.com/gin-gonic/gin"
	"github.com/urfave/cli/v2"
)

//脚本路径
var file string

//脚本参数
var params string

//钩子运行的端口, 默认8080
var port string = "8080"

func main() {

	app := &cli.App{
		Flags: []cli.Flag{
			&cli.StringFlag{
				Name:    "file",
				Aliases: []string{"f"},
				Usage:   "要执行的shell脚本路径, 参数必填，否则无法启动。",
			},
			&cli.StringFlag{
				Name:    "params",
				Aliases: []string{"s"},
				Usage:   "脚本接受的参数，参数可选。",
			},
			&cli.StringFlag{
				Name:    "port",
				Aliases: []string{"p"},
				Usage:   "钩子运行的端口，参数可选，默认8080。",
			},
		},
	}

	app.Action = func(c *cli.Context) error {

		file = c.String("file")
		params = c.String("params")

		// 脚本路径参数不传入不允许运行
		if len(file) == 0 {
			fmt.Println("出现错误：请先传入脚本执行路径！")
			os.Exit(1)
		}

		if len(c.String("port")) > 0 {
			port = c.String("port")
		}

		if !filepath.IsAbs(file) {
			file, _ = filepath.Abs(file)
		}

		r := gin.Default()
		r.GET("/", hook)
		r.POST("/hook", hook)
		// listen and serve on 0.0.0.0:8080
		r.Run(":" + port)
		return nil
	}

	err := app.Run(os.Args)
	if err != nil {
		log.Fatal(err)
	}
}

func execCommand(cmd *exec.Cmd) (stdout string, stderr string) {
	var stdoutBff bytes.Buffer
	var stderrBff bytes.Buffer

	cmd.Stdout = &stdoutBff
	cmd.Stderr = &stderrBff
	cmd.Run()

	fmt.Println(stdoutBff.String())
	fmt.Println(stderrBff.String())
	return stdoutBff.String(), stderrBff.String()
}

//hook
func hook(c *gin.Context) {

	var header map[string]string = make(map[string]string)

	for k, v := range c.Request.Header {
		fmt.Println(k, strings.Join(v, ","))
		header[k] = strings.Join(v, ",")
	}
	h, _ := json.Marshal(header)

	body,_ := ioutil.ReadAll(c.Request.Body)

	var cmdParams []string

	cmdParams = append(cmdParams, string(h))
	cmdParams = append(cmdParams, string(body))

	for _,value := range strings.Split(params," ") {
		cmdParams = append(cmdParams, value)
	}

	cmd := exec.Command(file, cmdParams...)

	go execCommand(cmd)

	c.String(http.StatusOK, fmt.Sprintln("调用成功，任务后台处理中!"))
}
