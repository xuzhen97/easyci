export GO111MODULE=on
export GOPROXY=https://goproxy.io,direct

EXEC=easyci
BUILD_DIR=build
BUILD_OS="linux"
BUILD_ARCH="amd64 386"
BUILD_FIERCE_CI="./main.go"

clean:
	@rm -f ${EXEC}
	@rm -f ${BUILD_DIR}/*

build: clean
	@go build --ldflags '-s' -i -o ${EXEC} ${BUILD_FIERCE_CI}

buildall: clean
	@mkdir -p ${BUILD_DIR}
	@for os in "${BUILD_OS}" ; do \
		for arch in "${BUILD_ARCH}" ; do \
			echo " * build $$os for $$arch"; \
			GOOS=$$os GOARCH=$$arch go build -ldflags "-s" -o ${BUILD_DIR}/${EXEC} ${BUILD_FIERCE_CI}; \
			cd ${BUILD_DIR}; \
			tar czf ${EXEC}.$$os.$$arch.tgz ${EXEC}; \
			cd - ; \
		done done
	@rm ${BUILD_DIR}/${EXEC}
